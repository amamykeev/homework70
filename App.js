import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

export default class App extends React.Component {

    state = {
        window: '',
        result: '',
    };

    removeNumbers = () => {
        this.setState({
            window: this.state.window.substr(0, this.state.window.length - 1),
        });
    };

    showValue = () => {
        this.setState({
            result: '',
            window: this.state.window.concat(value)
        })
    };

    addResult = () => {
        const result = eval(this.state.window);
        this.setState({
            window: '',
            result: result
        })
    };

    render() {

        const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '+', '-', '/', '*', '=', '<'];

        return (
            <View style={styles.container}>
                <View style={styles.window}>
                    <Text>
                        {this.state.window}
                        {this.state.result}
                    </Text>
                </View>
                <View style={styles.keyboard}>
                    {numbers.map((nmb, index) => {
                        if (nmb === '=') {
                            return (
                                <TouchableHighlight key={index} style={styles.button} onPress={this.addResult}>
                                    <Text>{nmb}</Text>
                                </TouchableHighlight>
                            )
                        } else {
                            return (
                                <TouchableHighlight key={index} style={styles.button} onPress={() => this.showValue(nmb)}>
                                    <Text>{nmb}</Text>
                                </TouchableHighlight>
                            )
                        };

                        if  (nmb === '<') {
                            return (
                                <TouchableHighlight key={index} style={styles.button} onPress={() => this.removeNumbers}>
                                    <Text>{nmb}</Text>
                                </TouchableHighlight>
                            )
                        } else {
                            return (
                                <TouchableHighlight key={index} style={styles.button} onPress={() => this.showValue(nmb)}>
                                    <Text>{nmb}</Text>
                                </TouchableHighlight>
                            )
                        }
                    })}
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 0,
    },

    button: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 1,
        width: 70,
        height: 70,
        backgroundColor: '#00ffff',
        borderColor: '#000',
        borderWidth: 1,
    },

    keyboard: {
        flexWrap: 'wrap',
        height: '50%',
        width: '100%',
        margin: '1%',
    }
});
